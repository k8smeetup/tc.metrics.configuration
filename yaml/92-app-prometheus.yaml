apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: prometheus
  name: prometheus
  namespace: development
  annotations:
    description: "prometheus"
    cb/app.version: "${ReleaseVersion}"
    cb/app.installdate: "__INSTALLDATE__"
    cb/git.hash: "__INSTALLHASH__"
spec:
  strategy:
      type: RollingUpdate
      rollingUpdate:
         maxSurge: 1
         maxUnavailable: 0
  revisionHistoryLimit: 5
  replicas: 1
  selector:
    matchLabels:
      app: prometheus
  template:
    metadata:
      labels:
        app: prometheus
        name: prometheus
        enabled: "true"
      annotations:
        deployment.kubernetes.io/maxreplicas: "3"
        cb/app.version: "${ReleaseVersion}"
        cb/app.installdate: "__INSTALLDATE__"
        cb/git.hash: "__INSTALLHASH__"
        enabled: "true"
    spec:
      # Anti-affinity rule to start the same pod on different nodes
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: beta.kubernetes.io/arch
                operator: In
                values:
                - amd64
              - key: beta.kubernetes.io/os
                operator: In
                values:
                - linux
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 100
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                - key: name
                  operator: In
                  values:
                  - prometheus
              topologyKey: kubernetes.io/hostname
      terminationGracePeriodSeconds: 15
      restartPolicy: Always
      #nodeSelector:
        #cb/windows.kernelrelease: "1803"
        #workload-generic: "true"      
      containers:
      - name: prometheus
        image: prom/prometheus:v2.2.1
        args: ["--config.file=/etc/prometheus/prometheus.yml", "--storage.tsdb.path=/prometheus/", "--web.enable-lifecycle"]
        imagePullPolicy: Always      
        env:
          - name: K8S_NODE
            valueFrom:
              fieldRef:
                fieldPath: spec.nodeName
          - name: K8S_NAMESPACE
            valueFrom:
              fieldRef:
                fieldPath: metadata.namespace
          - name: K8S_POD
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: K8S_POD_IP
            valueFrom:
              fieldRef:
                fieldPath: status.podIP
          - name: K8S_CONTAINER
            value: "prometheus"
          - name: K8S_ENV
            value: "development"          
        volumeMounts:
        - name: logdir
          mountPath: /logs/   
        - name: prometheus-config-volume
          mountPath: /etc/prometheus/
        - name: prometheus-storage-volume
          mountPath: /prometheus/             
        ports:
        - containerPort: 9090
          name: web-port
          protocol: TCP
        resources:
          limits:
            cpu: 1
            memory: 256Mi
          requests:
            cpu: 10m
            memory: 256Mi
        readinessProbe:
          httpGet:
            path: /
            port: 9090
            scheme: HTTP
          periodSeconds: 5
          timeoutSeconds: 16
          successThreshold: 2
          failureThreshold: 2
          initialDelaySeconds: 0     
      imagePullSecrets:
      - name: regcred   
      volumes:
      - name: k8s-storage
        emptyDir: {}
      - name: logdir
        hostPath:
          path: /logs/development/prometheus
          type: DirectoryOrCreate
      - name: prometheus-config-volume
        configMap:
          defaultMode: 420
          name: prometheus-server-conf
      - name: prometheus-storage-volume
        emptyDir: {}